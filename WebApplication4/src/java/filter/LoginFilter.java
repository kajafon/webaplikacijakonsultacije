/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package filter;

import com.sun.net.httpserver.Filter;
import com.sun.net.httpserver.HttpExchange;
import domen.Profesor;
import domen.Student;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import mb.MBSesija;

/**
 *
 * @author Katarina
 */
//@WebFilter("*.xhtml")
public class LoginFilter implements javax.servlet.Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
          }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest req = (HttpServletRequest) request;
    String url = req.getContextPath()+"/faces/index.xhtml";
        MBSesija sesija = (MBSesija) req.getSession().getAttribute("mbSesija");
        Student student = (sesija!=null)? sesija.getUlogovaniKorisnik():null;
        Profesor profesor = (sesija!=null)? sesija.getUlogovaniProfesor():null;
HttpServletResponse res = (HttpServletResponse) response;
//res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        //    res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
         //   res.setDateHeader("Expires", 0); 
        if ( student==null  && profesor==null && !req.getRequestURI().equalsIgnoreCase(url)) {
            // User is logged in, so just continue request.
            
            res.sendRedirect(req.getContextPath() + "/faces/index.xhtml");
           
        } else if(req.getRequestURI().equalsIgnoreCase(url) ){
           
            if(profesor!=null){
             res.sendRedirect(req.getContextPath() + "/faces/profesor.xhtml");
            }
            if(student!=null){
             res.sendRedirect(req.getContextPath() + "/faces/pocetna.xhtml");
            }
        }
        
        else{
            
            
            // User is not logged in, so redirect to index.
             chain.doFilter(request, res);
           
        }   
    
    }

    @Override
    public void destroy() {
          }

   
    
}
