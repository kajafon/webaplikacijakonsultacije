/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konverteri;

import domen.Predmet;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import sb.predmet.SBPredmetLocal;


/**
 *
 * @author Katarina
 */
@FacesConverter(value = "predmetKonverter")
public class PredmetKonverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
     if (value == null || value.isEmpty()) {
            return null;    
    }
     
     Integer predmetID = Integer.parseInt(value);
        Predmet predmetDB = null;
        try {
            SBPredmetLocal sbPredmet = (SBPredmetLocal) new InitialContext().lookup("java:global/WebApplication4/SBPredmet");

            predmetDB = sbPredmet.vratiPredmetZaID(predmetID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return predmetDB;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value instanceof Predmet) {
            Predmet predmet = (Predmet) value;
            if (predmet != null) {
                return String.valueOf(predmet.getIdPredmeta());
            }
        }
        return "";

    }   
}
