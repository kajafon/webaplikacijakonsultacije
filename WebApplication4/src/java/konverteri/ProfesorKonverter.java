/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konverteri;

import domen.Profesor;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import sb.profesor.SBProfesorLocal;

/**
 *
 * @author Katarina
 */
@FacesConverter(value = "profesorKonverter")
public class ProfesorKonverter implements Converter{

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
     if (value == null || value.isEmpty()) {
            return null;    
    }
     
     Integer profesorID = Integer.parseInt(value);
        Profesor profesorDB = null;
        try {
            SBProfesorLocal sbProfesor = (SBProfesorLocal) new InitialContext().lookup("java:global/WebApplication4/SBProfesor");

            profesorDB = sbProfesor.vratiProfesoraZaId(profesorID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return profesorDB;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value instanceof Profesor) {
            Profesor profesor = (Profesor) value;
            if (profesor != null) {
                return String.valueOf(profesor.getIdProfesora());
            }
        }
        return "";

    }   
}
