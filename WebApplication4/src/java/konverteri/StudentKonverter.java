/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package konverteri;

import domen.Predmet;
import domen.Student;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import sb.predmet.SBPredmetLocal;
import sb.student.SBStudentLocal;

/**
 *
 * @author Katarina
 */
@FacesConverter(value = "studentKonverter")
public class StudentKonverter implements Converter{
 
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
     if (value == null || value.isEmpty()) {
            return null;    
    }
     
     Integer studentID = Integer.parseInt(value);
        Student studentDB = null;
        try {
            SBStudentLocal sbstudent = (SBStudentLocal) new InitialContext().lookup("java:global/WebApplication4/SBStudent");

            studentDB = sbstudent.vratiStudentaZaID(studentID);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return studentDB;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value instanceof Student) {
            Student student = (Student) value;
            if (student != null) {
                return String.valueOf(student.getIdStudent());
            }
        }
        return "";

    }    
}
