/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Profesor;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import sb.konsultacije.SBKonsultacijeLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@ViewScoped
public class MBDodajKonsultacije implements Serializable{

     @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;
     
     
     @EJB
     SBKonsultacijeLocal sbKonsultacije;

    public SBKonsultacijeLocal getSbKonsultacije() {
        return sbKonsultacije;
    }

    public void setSbKonsultacije(SBKonsultacijeLocal sbKonsultacije) {
        this.sbKonsultacije = sbKonsultacije;
    }
     
     
    public MBDodajKonsultacije() {
    }
    
    Date datum;
    int period;
    Date vremeOd;
    Date vremeDo;
    int satiOd;
    int minOd;
    int satiDo;
    int minDo;

    public int getSatiOd() {
        return satiOd;
    }

    public void setSatiOd(int satiOd) {
        this.satiOd = satiOd;
    }

    public int getMinOd() {
        return minOd;
    }

    public void setMinOd(int minOd) {
        this.minOd = minOd;
    }

    public int getSatiDo() {
        return satiDo;
    }

    public void setSatiDo(int satiDo) {
        this.satiDo = satiDo;
    }

    public int getMinDo() {
        return minDo;
    }

    public void setMinDo(int minDo) {
        this.minDo = minDo;
    }
    
    

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

     
     public String dodajKonsultacije(){
    // if(vremeOd.after(vremeDo)){
      FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
     String dateStr = "Mon Jun 18 00:00:00 IST 2012";

Date date=null;
         try {
             date = (Date)format.parse(datum.toString());
         } catch (ParseException ex) {
             Logger.getLogger(MBDodajKonsultacije.class.getName()).log(Level.SEVERE, null, ex);
         }
System.out.println(date);        

Calendar cal = Calendar.getInstance();
cal.setTime(date);
String formatedDate = cal.get(Calendar.YEAR) + "." + (cal.get(Calendar.MONTH) + 1) + "." +         cal.get(Calendar.DATE);
System.out.println("formatedDate : " + formatedDate);
        sbKonsultacije.dodajKonsultacije(formatedDate,satiOd+":"+minOd,satiDo+":"+minDo,period,mbSesija.ulogovaniProfesor);
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Obavestenje", "Konsultacije su dodate!")); 
        return "dodateKonsultacije";
     }
    
    
}
