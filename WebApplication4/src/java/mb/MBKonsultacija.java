/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mb;

import domen.Konsultacija;
import domen.Profesor;
import domen.Student;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.SelectEvent;
import sb.konsultacije.SBKonsultacijeLocal;
import sb.profesor.SBProfesorLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@SessionScoped
public class MBKonsultacija implements Serializable {

    @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;
    
     public MBKonsultacija() {

    }
    @EJB
    SBKonsultacijeLocal sbKonsultacije;

    @EJB
    SBProfesorLocal sbProfesor;

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }

    String datumi;

    public String getDatumi() {
        return datumi;
    }

    public void setDatumi(String datumi) {
        this.datumi = datumi;
    }
    
    
    private Date date1;
    String date2;

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

   
    Profesor selektovaniProfesor;
    private List<Profesor> listaProfesora;
    private List<Konsultacija> listaKonsultacija;
   
    public Profesor getSelektovaniProfesor() {
        return selektovaniProfesor;
    }

    public void setSelektovaniProfesor(Profesor selektovaniProfesor) {
        this.selektovaniProfesor = selektovaniProfesor;
    }

    public List<Profesor> getListaProfesora() {
        return listaProfesora;
    }

    public void setListaProfesora(List<Profesor> listaProfesora) {
        this.listaProfesora = listaProfesora;
    }

    public List<Konsultacija> getListaKonsultacija() {
        return listaKonsultacija;
    }

    public void setListaKonsultacija(List<Konsultacija> listaKonsultacija) {
        this.listaKonsultacija = listaKonsultacija;
    }

    @PostConstruct
    public void vratiKonsultacije() {
        date1 = new Date();
        date2 = date1.toString();
        // listaKonsultacija = sbKonsultacije.vratiKonsultacije();
        listaProfesora = sbProfesor.vratiSveProfesore();
      selektovaniProfesor=listaProfesora.get(0);
      List<Date> lista = sbKonsultacije.vratiDatumeKonsultacijaZaProfesora(selektovaniProfesor);
        prikaziDatumeZaProfesora(lista);
      // listaKonsultacija = sbKonsultacije.vratiKonsultacijeZaDatumIProfesora(date1,null);
        // listaKonsultacija = sbKonsultacije.vratiKonsultacijeZaDatumIProfesora(datum, p);

    }
    public void prikaziDatumeZaProfesora(List<Date> listaDatuma){
    datumi = "";
     FacesContext facesContext = FacesContext.getCurrentInstance();
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selectovani profesor ima konsultacije u sledecim terminima:","");
      facesContext.addMessage("",msg);
        for (int i = 0; i < listaDatuma.size(); i++) {
            //datumi+=listaDatuma.get(i).toString()+"<br/>";
            msg=new FacesMessage(FacesMessage.SEVERITY_INFO, "",listaDatuma.get(i).toString());
      
            facesContext.addMessage("", msg);
        }
       // facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Selectovani profesor ima konsultacije u sledecim terminima:",datumi));
        
    }
     public void handleDateSelect(SelectEvent event) {
       
        SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
        date2 = format.format(event.getObject());
        date1 = (Date) event.getObject();

        listaKonsultacija = sbKonsultacije.vratiKonsultacijeZaDatumIProfesora(date1, selektovaniProfesor);
      //  if(date1==null || selektovaniProfesor == null)
        //  listaKonsultacija = sbKonsultacije.vratiKonsultacijeZaDatumIProfesora(date1, selektovaniProfesor);
        //   else
        //listaKonsultacija = sbKonsultacije.vratiKonsultacije();  
    }

    public void vratiKonsultacijeZaDatumIProfesora() {

        listaKonsultacija = sbKonsultacije.vratiKonsultacijeZaDatumIProfesora(date1, selektovaniProfesor);
         List<Date> lista = sbKonsultacije.vratiDatumeKonsultacijaZaProfesora(selektovaniProfesor);
        prikaziDatumeZaProfesora(lista);
        
        
    }

    public String pokreniIzmenu(Konsultacija k) {
        if(k.getStudent()==null){
            k.setStudent(mbSesija.ulogovaniStudent);
        mbSesija.getMapaPodataka().put("konsultacija", k);
        return "zakazi";
        }
        else {
            try{
        sbKonsultacije.otkaziKonsultaciju(k);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Obavestenje", "Konsultacija je otkazana!"));
        return "pocetna.xhtml";
            } catch(Exception e){
              FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Greska!", "Pokusajte ponovo")); 
            }
        
        }
        return "";
    }

    public boolean disabledZakazi(Konsultacija k) {
        if ((k.getStudent() == null || k.getStudent().equals(mbSesija.getUlogovaniKorisnik()))&& k.getKonsultacijaPK().getDatum().after(new Date())) {
            return false;
        }
        return true;
    }

    public String natpisNaDugmetu(Konsultacija k) {
       String natpis = "Zakazi";
       Student s = mbSesija.getUlogovaniKorisnik();
       if(k.getStudent()!=null && k.getStudent().equals(s)){
       return "Otkazi";
       }
        return natpis;
    }
    
    public String odjaviStudenta(){
     mbSesija.setUlogovaniKorisnik(null);
        return "index.xhtml";
    }
}
