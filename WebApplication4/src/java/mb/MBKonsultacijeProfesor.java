/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Konsultacija;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import sb.konsultacije.SBKonsultacijeLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@SessionScoped
public class MBKonsultacijeProfesor implements Serializable{

    @EJB
    SBKonsultacijeLocal sbKonsultacija;
    
    @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;

    public SBKonsultacijeLocal getSbKonsultacija() {
        return sbKonsultacija;
    }

    public void setSbKonsultacija(SBKonsultacijeLocal sbKonsultacija) {
        this.sbKonsultacija = sbKonsultacija;
    }
    
    public MBKonsultacijeProfesor() {
    }
    
    private List<Konsultacija> listaKonsultacija;
     Date datum;

    public List<Konsultacija> getListaKonsultacija() {
        return listaKonsultacija;
    }

    public void setListaKonsultacija(List<Konsultacija> listaKonsultacija) {
        this.listaKonsultacija = listaKonsultacija;
    }
    
    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
    
    @PostConstruct
    public void init(){
    datum = new Date();
    listaKonsultacija = sbKonsultacija.vratiKonsultacijeZaDatumIProfesora(datum, mbSesija.getUlogovaniProfesor());
    }
    
     public void handleDateSelect(SelectEvent event) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        SimpleDateFormat format = new SimpleDateFormat("d/M/yyyy");
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Date Selected", format.format(event.getObject())));
        datum = (Date) event.getObject();

        listaKonsultacija = sbKonsultacija.vratiKonsultacijeZaDatumIProfesora(datum, mbSesija.getUlogovaniProfesor());
       
    }

   // public MBSesija getMbSesija() {
   //     return mbSesija;
   // }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }
     
    public String odjaviProfesora(){
        mbSesija.setUlogovaniProfesor(null);
       // mbSesija.setMapaPodataka(null);
        //setDatum(null);
        return "index.xhtml";
    }
    
    public String natpisNaDugmetu(Konsultacija k) {
       String natpis = "Otkazi";
       
       if(k.getKonsultacijaPK().getDatum().before(new Date())){
       return "Unesi komentar";
       }
       return natpis;
    }
    
     public String pokreniIzmenu(Konsultacija k) {
        if(k.getKonsultacijaPK().getDatum().before(new Date())){
          //  otvoriFormuDodajKomentar(k);
            konsultacijaZaZakazivanje=k;
        return "dodajKomentar";
        }
        else {
            try{
        sbKonsultacija.obrisiKonsultaciju(k);//iymeniti ya profesora
        listaKonsultacija.remove(k);
        FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Obavestenje", "Konsultacija je otkazana!"));
        return "ostaniNaProfesoru";
            } catch(Exception e){
              FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Greska!", "Pokusajte ponovo")); 
            }
        
        }
        return "";
    }
     Konsultacija konsultacijaZaZakazivanje;

    public Konsultacija getKonsultacijaZaZakazivanje() {
        return konsultacijaZaZakazivanje;
    }

    public void setKonsultacijaZaZakazivanje(Konsultacija konsultacijaZaZakazivanje) {
        this.konsultacijaZaZakazivanje = konsultacijaZaZakazivanje;
    }
     
     public void otvoriFormuDodajKomentar(Konsultacija k) {
         konsultacijaZaZakazivanje=k;
        RequestContext.getCurrentInstance().openDialog("dodajKomentar");  
    }  
     
     public String dodajKomentar(){
         try{
     sbKonsultacija.dodajKomentarProfesora(konsultacijaZaZakazivanje);
       return "profesor";  
         }catch (Exception e){
           FacesContext facesContext = FacesContext.getCurrentInstance();
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Obavestenje", "Dodavanje komentara nije uspelo!"));
       
                return "";}

       }
    
       }

