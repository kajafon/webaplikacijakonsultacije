/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Profesor;
import domen.Student;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import sb.profesor.SBProfesorLocal;
import sb.student.SBStudentLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean(name = "mbLogovanje")
@RequestScoped
public class MBLogovanje implements Serializable{

   private Student tekuciKorisnik;
    @EJB
    SBStudentLocal sbKorisnik;
    
    @EJB
    SBProfesorLocal sbProfesor;

    public SBStudentLocal getSbKorisnik() {
        return sbKorisnik;
    }

    public void setSbKorisnik(SBStudentLocal sbKorisnik) {
        this.sbKorisnik = sbKorisnik;
    }

    public SBProfesorLocal getSbProfesor() {
        return sbProfesor;
    }

    public void setSbProfesor(SBProfesorLocal sbProfesor) {
        this.sbProfesor = sbProfesor;
    }
    
    @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }
    
    
    public MBLogovanje() {
        tekuciKorisnik = new Student();
    }

    public Student getTekuciKorisnik() {
        return tekuciKorisnik;
    }

    public void setTekuciKorisnik(Student tekuciKorisnik) {
        this.tekuciKorisnik = tekuciKorisnik;
    }

   
    
    
    public String prijaviKorisnika(){
        
       try{
          //if(tekuciKorisnik.getEmail().equalsIgnoreCase("kaja")){
              
        tekuciKorisnik = sbKorisnik.vratiStudenta(tekuciKorisnik);
        mbSesija.setUlogovaniKorisnik(tekuciKorisnik);
        }catch(Exception ex){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            return null;
        }

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Корисник је успесно улогован!", ""));
       return "ulogujStudenta";}
       
    public String prijaviSeKaoProfesor(){
        
       try{
           Profesor trenutniProfesor = new Profesor();
           trenutniProfesor.setEmailProfesora(tekuciKorisnik.getEmailStudenta());
           trenutniProfesor.setSifraProfesora(tekuciKorisnik.getSifraStudenta());
              trenutniProfesor = sbProfesor.vratiProfesora(trenutniProfesor);
       // tekuciKorisnik = sbKorisnik.vratiStudenta(tekuciKorisnik);
       // mbSesija.setUlogovaniKorisnik(tekuciKorisnik);
            mbSesija.setUlogovaniProfesor(trenutniProfesor);
        }catch(Exception ex){
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage(), ""));
            return null;
        }

        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Корисник је успесно улогован!", ""));
       return "ulogujProfesora";}
    
    
}
