/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Profesor;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import sb.profesor.SBProfesorLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@SessionScoped
public class MBProfesor {

    @EJB
    SBProfesorLocal sbProfesor;
    /**
     * Creates a new instance of MBProfesor
     */
    public MBProfesor() {
    }
    
    private List<Profesor> listaProfesora;
    Profesor selektovaniProfesor;

    public Profesor getSelektovaniProfesor() {
        return selektovaniProfesor;
    }

    public void setSelektovaniProfesor(Profesor selektovaniProfesor) {
        this.selektovaniProfesor = selektovaniProfesor;
    }
  
    public List<Profesor> getListaProfesora() {
        return listaProfesora;
    }

    public void setListaProfesora(List<Profesor> listaProfesora) {
        this.listaProfesora = listaProfesora;
    }
@PostConstruct
public void vratiListuPredmeta(){
listaProfesora = sbProfesor.vratiSveProfesore();
}
}
