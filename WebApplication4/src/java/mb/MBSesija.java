/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;


import domen.Profesor;
import domen.Student;
import java.io.Serializable;
import java.util.HashMap;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Katarina
 */
@ManagedBean(name = "mbSesija")
@SessionScoped
public class MBSesija implements Serializable{

    Student ulogovaniStudent;
   Profesor ulogovaniProfesor;

    

   public Profesor getUlogovaniProfesor() {
       return ulogovaniProfesor;
   }

 public void setUlogovaniProfesor(Profesor ulogovaniProfesor) {
  this.ulogovaniProfesor = ulogovaniProfesor;
   }
     
    
    HashMap<String, Object> mapaPodataka;
    /**
     * Creates a new instance of mbSesija
     */
    public MBSesija() {
        mapaPodataka = new HashMap<String, Object>();
    }

    public Student getUlogovaniKorisnik() {
        return ulogovaniStudent;
    }

    public void setUlogovaniKorisnik(Student ulogovaniKorisnik) {
      this.ulogovaniStudent= ulogovaniKorisnik;
    }

    public HashMap<String, Object> getMapaPodataka() {
        return mapaPodataka;
    }

    public void setMapaPodataka(HashMap<String, Object> mapaPodataka) {
        this.mapaPodataka = mapaPodataka;
    }
    
    
}
