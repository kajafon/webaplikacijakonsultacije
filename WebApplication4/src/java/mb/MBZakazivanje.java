/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Konsultacija;
import domen.Predmet;
import domen.Profesorpredmet;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import sb.konsultacije.SBKonsultacijeLocal;
import sb.predmet.SBPredmet;
import sb.predmet.SBPredmetLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@ViewScoped
public class MBZakazivanje implements Serializable{
    
    @EJB
    SBKonsultacijeLocal sbKonsultacija;
    
    @EJB
    SBPredmetLocal sbPredmet;

    public SBPredmetLocal getSbPredmet() {
        return sbPredmet;
    }

    public void setSbPredmet(SBPredmetLocal sbPredmet) {
        this.sbPredmet = sbPredmet;
    }
    
    
  
    @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;
    
    private Konsultacija konsultacijaZaZakazivanje;
    private Predmet selektovaniPredmet;
    private List<Profesorpredmet> listaPredmeta;

    public List<Profesorpredmet> getListaPredmeta() {
        return listaPredmeta;
    }

    public void setListaPredmeta(List<Profesorpredmet> listaPredmeta) {
        this.listaPredmeta = listaPredmeta;
    }
    

    public SBKonsultacijeLocal getSbKonsultacija() {
        return sbKonsultacija;
    }

    public void setSbKonsultacija(SBKonsultacijeLocal sbKonsultacija) {
        this.sbKonsultacija = sbKonsultacija;
    }

    public Predmet getSelektovaniPredmet() {
        return selektovaniPredmet;
    }

    public void setSelektovaniPredmet(Predmet selektovaniPredmet) {
        this.selektovaniPredmet = selektovaniPredmet;
    }
    

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }

    
    public Konsultacija getKonsultacijaZaZakazivanje() {
        return konsultacijaZaZakazivanje;
    }

    public void setKonsultacijaZaZakazivanje(Konsultacija konsultacijaZaZakazivanje) {
        this.konsultacijaZaZakazivanje = konsultacijaZaZakazivanje;
    }

    
    public MBZakazivanje() {
    }
    
    @PostConstruct
    public void pokreniFormu(){
    konsultacijaZaZakazivanje=(Konsultacija) mbSesija.mapaPodataka.get("konsultacija");
    if(konsultacijaZaZakazivanje ==null){
        konsultacijaZaZakazivanje = new Konsultacija();
    }
    listaPredmeta= vratiPredmeteZaProfesora();
    mbSesija.mapaPodataka.remove("konsultacija");
    }
    
    public String zakaziKonsultaciju(){
        try {
            konsultacijaZaZakazivanje.setStudent(mbSesija.getUlogovaniKorisnik());
            sbKonsultacija.zakaziKonsultaciju(konsultacijaZaZakazivanje);
        } catch (Exception ex) {
            Logger.getLogger(MBZakazivanje.class.getName()).log(Level.SEVERE, null, ex);
             FacesContext facesContext = FacesContext.getCurrentInstance();  
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Poruka",ex.getMessage() ));
        return null;
            
        }
        return "zakazanaKonsultacija";
    }
    
    public List<Profesorpredmet> vratiPredmeteZaProfesora(){
        try {
            listaPredmeta=sbPredmet.vratiPredmeteZaProfesora(konsultacijaZaZakazivanje.getProfesor1());
        } catch (Exception ex) {
            Logger.getLogger(MBZakazivanje.class.getName()).log(Level.SEVERE, null, ex);
             FacesContext facesContext = FacesContext.getCurrentInstance();  
        facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Poruka",ex.getMessage() ));
        return null;
            
        }
        return listaPredmeta;
    }
}
