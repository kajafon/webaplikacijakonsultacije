/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Konsultacija;
import domen.Student;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import sb.konsultacije.SBKonsultacijeLocal;
import sb.student.SBStudentLocal;

/**
 *
 * @author Katarina
 */
@ManagedBean
@SessionScoped
public class mbPregledPoStudentu implements Serializable{

    /**
     * Creates a new instance of mbPregledPoStudentu
     */
    
     @ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;
    public mbPregledPoStudentu() {
    }
    @EJB
    SBKonsultacijeLocal sbKonsultacije;
     @EJB
     SBStudentLocal sbStudent;
    
    Student odabraniStudent;
    
    List<Student> listaStudenata;
    
    String imeIPrezime;

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }

    public SBKonsultacijeLocal getSbKonsultacije() {
        return sbKonsultacije;
    }

    public void setSbKonsultacije(SBKonsultacijeLocal sbKonsultacije) {
        this.sbKonsultacije = sbKonsultacije;
    }

    public Student getOdabraniStudent() {
        return odabraniStudent;
    }

    public void setOdabraniStudent(Student odabraniStudent) {
        this.odabraniStudent = odabraniStudent;
    }

    public List<Student> getListaStudenata() {
        return listaStudenata;
    }

    public void setListaStudenata(List<Student> listaStudenata) {
        this.listaStudenata = listaStudenata;
    }

    public String getImeIPrezime() {
        return imeIPrezime;
    }

    public void setImeIPrezime(String imeIPrezime) {
        this.imeIPrezime = imeIPrezime;
    }
    
    public List<Student> vratiStudente(String ime){
    List<Student> lista =sbStudent.vratiStudente(ime);
    listaStudenata = lista;
    return lista;
    }
    
    @PostConstruct
    public void init(){
    listaStudenata = sbStudent.vratiSveStudente();
        vratiListuKonsultacija();
    }
    List<Konsultacija> listaKonsultacija;

    public List<Konsultacija> getListaKonsultacija() {
        return listaKonsultacija;
    }

    public void setListaKonsultacija(List<Konsultacija> listaKonsultacija) {
        this.listaKonsultacija = listaKonsultacija;
    }
    
    public void vratiListuKonsultacija(){
   listaKonsultacija= sbKonsultacije.vratiKonsultacijeZaProfesoraIStudenta(odabraniStudent, mbSesija.ulogovaniProfesor);
 
    }
}
