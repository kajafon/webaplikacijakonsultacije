/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mb;

import domen.Preporuka;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import sb.preporuke.sbPreporuke;
import sb.preporuke.sbPreporukeLocal;

/**
 *
 * @author Kaja
 */
@ManagedBean
@SessionScoped
public class mbPreporuka {
@ManagedProperty(value = "#{mbSesija}")
    MBSesija mbSesija;

@EJB
sbPreporukeLocal sbPreporuke;
    /**
     * Creates a new instance of mbPreporuka
     */
    public mbPreporuka() {
    }
    public List<Preporuka> listaPreporuka;

    public MBSesija getMbSesija() {
        return mbSesija;
    }

    public void setMbSesija(MBSesija mbSesija) {
        this.mbSesija = mbSesija;
    }

    public sbPreporukeLocal getSbPreporuke() {
        return sbPreporuke;
    }

    public void setSbPreporuke(sbPreporukeLocal sbPreporuke) {
        this.sbPreporuke = sbPreporuke;
    }

    public List<Preporuka> getListaPreporuka() {
        return listaPreporuka;
    }

    public void setListaPreporuka(List<Preporuka> listaPreporuka) {
        this.listaPreporuka = listaPreporuka;
    }
    
    
    @PostConstruct
    public void napuniListuPreporuka(){
    // HashMap<String,Object> mapaPreporuka = sbPreporuke.vratiSmeroveZaMaster(null);
       HashMap<String,Object> mapaPreporuka = recommendation.core.getRecommendations(1, 1);
        List<Preporuka> lista = new ArrayList<Preporuka>();
       for(Entry<String, Object> entry : mapaPreporuka.entrySet()) {
    String key = entry.getKey();
    Object value = entry.getValue();
           switch (key) {
               case "1": 
                   Preporuka p = new Preporuka();
                   p.setBrojSmera(1);
                   p.setOpisSmera("SIRN");
                   p.setProsekOcena((Double)value);
                   lista.add(p);
                   break;
               case "2":
                    Preporuka p1 = new Preporuka();
                   p1.setBrojSmera(2);
                   p1.setOpisSmera("ISIT");
                   p1.setProsekOcena((Double)value);
                   lista.add(p1);
                   break;
                   case "3":
                    Preporuka p2 = new Preporuka();
                   p2.setBrojSmera(3);
                   p2.setOpisSmera("EPOS");
                   p2.setProsekOcena((Double)value);
                   lista.add(p2);
                   break;
                       case "4":
                    Preporuka p3 = new Preporuka();
                   p3.setBrojSmera(4);
                   p3.setOpisSmera("OIRS");
                   p3.setProsekOcena((Double)value);
                   lista.add(p3);
                   break;
              
           }

}
       
      
       listaPreporuka=lista;
    }}
